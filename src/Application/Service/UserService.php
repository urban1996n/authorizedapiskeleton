<?php

namespace App\Application\Service;

use App\Domain\Model\User\User;
use App\Domain\Model\User\UserRepositoryInterface;
use Doctrine\ORM\EntityNotFoundException;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
    
/**
 * Class UserService
 * @package App\Application\Service
 */
final class UserService
{
    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    /**
     * UserService constructor.
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(UserRepositoryInterface $userRepository){
        $this->userRepository = $userRepository;
    }

    /**
     * @param int $userId
     * @return User
     * @throws EntityNotFoundException
     */
    public function getUser(int $userId): User
    {
        $user = $this->userRepository->findById($userId);
        if (!$user) {
            throw new EntityNotFoundException('User has not been found');
        }
        return $user;
    }

    /**
     * @param string $apiKey
     * @return User
     * @throws EntityNotFoundException | 
     */
    public function authenticate(string $apiKey, string $username , string $password): User
    {
        $user = $this->userRepository->findByApiKey($apiKey);

        if (!$user) {
            throw new EntityNotFoundException('User has not been found');
        }

        if($user->getApiKey() !==$apiKey || $user->getUsername() !==$username || $user->getPassword() !== hash('sha512',$password)){
            throw new BadCredentialsException("Given credentials does not match any Account. Check it before you try again.");
        }
        
        return $user;
    }

    /**
     * @return array|null
     */
    public function getAllUsers(): ?array
    {
        return $this->userRepository->findAll();
    }

    /**
     * @param string $title
     * @param string $content
     * @return User
     */
    public function addUser(string $username, string $password, string $role): User
    {
        $apiKey = substr(str_shuffle(MD5(microtime())), 0, 10);
        $user = new User();
        if($this->userRepository->findByUsername($username) !== null){
            throw new \InvalidArgumentException("Username with name {$username} already exists. Pick another username");            
        }
        $user->setUsername($username);
        $user->setPassword($password);
        $user->setRole($role);
        $user->setApiKey($apiKey);
        $this->userRepository->save($user);

        return $user;
    }

    /**
     * @param int $userId
     * @param string $title
     * @param string $content
     * @return User
     * @throws EntityNotFoundException
     */
    public function updateUser(int $userId, string $username, string $password, string $role ): User
    {
        $user = $this->userRepository->findById($userId);
        if (!$user) {
            throw new EntityNotFoundException('User has not been found');            
        }
        if($username !== null){
            $user->setUsername($username);
        }
        if($password !== null){
            $user->setPassword($password);
        }
        if($role !== null){        
            $user->setRole($role);
        }
        $this->userRepository->save($user);

        return $user;
    }

    /**
     * @param int $userId
     * @throws EntityNotFoundException
     */
    public function deleteUser(int $userId): void
    {
        $user = $this->userRepository->findById($userId);
        if (!$user) {
            throw new EntityNotFoundException('User has not been found');            
        }

        $this->userRepository->delete($user);
    }

}
